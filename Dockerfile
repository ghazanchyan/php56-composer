FROM ubuntu:16.04

MAINTAINER Aram Ghazanchyan <ghazanchyan@gmail.com>

ENV LANG=C.UTF-8
ENV DEFAULT_LOCALE=en_US
ENV DEBIAN_FRONTEND=noninteractive
ENV DOCUMENT_ROOT=/app/public

ADD docker /docker

RUN apt-get update &&\
    apt-get upgrade -y &&\
    apt-get install -y software-properties-common &&\
    add-apt-repository -y ppa:ondrej/php &&\
    add-apt-repository -y ppa:nginx/stable &&\
    apt-get update &&\
    apt-get install -y \
      supervisor nginx curl git \
      php5.6-fpm php5.6-mysql php5.6-zip php5.6-json php5.6-curl php5.6-gd php5.6-intl php5.6-mcrypt php5.6-mbstring php5.6-sqlite php5.6-tidy php5.6-xmlrpc php5.6-xsl php5.6-pgsql php5.6-mongo php5.6-ldap php5.6-cli \
      php-pear php-igbinary php-mongo php-redis &&\
    apt-get purge -y --auto-remove software-properties-common

# Node 6 install
RUN curl -sL https://deb.nodesource.com/setup_12.x | bash -
RUN apt-get install -y nodejs

RUN curl -s https://getcomposer.org/installer | php &&\
    mv composer.phar /usr/local/bin/composer

RUN rm /etc/nginx/sites-enabled/* &&\
    cp /docker/nginx/nginx_vhost /etc/nginx/sites-available/ &&\
    ln -s /etc/nginx/sites-available/nginx_vhost /etc/nginx/sites-enabled/nginx_vhost

RUN cp /docker/fpm/php-fpm.conf /etc/php/5.6/fpm/php-fpm.conf &&\
    cp /docker/fpm/www.conf /etc/php/5.6/fpm/pool.d/www.conf &&\
    cp /docker/supervisor/supervisord.conf /etc/supervisord.conf

RUN mkdir /app &&\
    chown -R www-data:www-data /app

RUN rm -R /docker

EXPOSE 80

ENTRYPOINT ["/usr/bin/supervisord", "-n", "-c", "/etc/supervisord.conf"]