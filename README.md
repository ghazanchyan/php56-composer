Docker PHP-5.6/Composer/Node
==============

Image contains:

* ubuntu 16.04
* nginx (latest)
* node, npm
* php 5.6
* php-fpm
* pear
* composer

php modules installed:

* mysql
* redis
* curl
* json
* mbstring
* gd
* xml
* zip
* intl 
* mcrypt 
* tidy 
* mongo

Usage
-----

Run a container with:

    $ docker run -d -p 8081:80 ghazanchyan/php56
    
Document root by default: /app/public

    $ ENV DOCUMENT_ROOT=/app/public

This image can be used as base image for php projects. 

License
-------

BSD
